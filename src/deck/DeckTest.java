package deck;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeckTest {
    private Deck deck;

    @BeforeEach
    void setUp() {
        deck = Deck.getInstance();
    }

    @Test
    void assertSingleDeckInstance() {
        Deck newDeck = Deck.getInstance();
        assertSame(deck, newDeck);
    }
}