package deck;

public class Card {
    private Suit suit;
    private int number;

    public Card(Suit s, int n) {
        if((n < 2) || (n > 14)) {
            throw new IllegalArgumentException("Invalid number.");
        }

        suit = s;
        number = n;
    }

    public String toString() {
        String stringRepr;

        switch(number) {
            case 11:
                stringRepr = "Jack";
                break;
            case 12:
                stringRepr = "Queen";
                break;
            case 13:
                stringRepr = "King";
                break;
            case 14:
                stringRepr = "Ace";
                break;
            default:
                stringRepr = Integer.toString(number);
                break;
        }

        stringRepr += " of " + suit;

        return stringRepr;
    }
}
