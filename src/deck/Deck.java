package deck;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Deck {
    //region Singleton
    private static Deck DECK_INSTANCE;

    public static Deck getInstance() {
        if (DECK_INSTANCE == null) {
            DECK_INSTANCE = new Deck();
        }

        return DECK_INSTANCE;
    }
    //endregion Singleton

    private List<Card> cards;

    private Deck() {
        cards = new ArrayList<>();

        // build the deck
        for (Suit suit : Suit.values()) {
            for (int i = 2; i <= 14; i++) {
                cards.add(new Card(suit, i));
            }
        }

        // shuffle it!
        Collections.shuffle(cards, new Random());
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (Card card : cards) {
            sb.append(card).append("\n");
        }

        return sb.toString();
    }
}
